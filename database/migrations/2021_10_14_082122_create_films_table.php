<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('film', function (Blueprint $table) {
            $table->string('kd_film', 100);
            $table->string('nm_film', 55);
            $table->string('genre', 55);
            $table->string('artis', 55);
            $table->string('produser', 55);
            $table->bigInteger('pendapatan')->unsigned();
            $table->integer('nominasi')->unsigned();
            $table->primary('kd_film');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('film');
    }
}
