@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jawaban Lima</div>
                <div class="card-body">
                    <iframe src="https://pastebin.com/embed_iframe/z8cUst95?theme=dark" style="border:none;width:100%; height:220px;"></iframe>
                    <h2>Hasil</h2>
                    <code>
                        {{ $hasil }}
                    </code>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
