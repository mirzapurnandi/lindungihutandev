@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jawaban Empat</div>
                <div class="card-body">
                    <iframe src="https://pastebin.com/embed_iframe/1M69Tprt?theme=dark" style="border:none; width:100%; height:600px;"></iframe>
                    <h2>Hasil</h2>
                    <code>
                        @foreach ($hasil as $val)
                            {{ $val. '' }}
                        @endforeach
                    </code>
                    <p>Tambahkan parameter angka 1 s.d 100 di url atas, terimakasih </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
