@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jawaban Dua</div>
                <div class="card-body">
                    <iframe src="https://pastebin.com/embed_iframe/Rc8EAhKf?theme=dark" style="border:none;width:100%;height:350px;"></iframe>
                    <h2>Hasil Menggunakan For</h2>
                    <code>{!! $hasil_satu !!}</code>

                    <hr>
                    <h2>Hasil Menggunakan Foreach</h2>
                    <code>{!! $hasil_dua !!}</code>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
