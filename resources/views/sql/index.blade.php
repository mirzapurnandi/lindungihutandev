@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Jawaban SQL</div>

                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @foreach ($result as $key => $val)

                            <li class="list-group-item">
                                <a href="{{ route('sql.pilihan', $val[0]) }}" @php echo $val[2] == 0 ? 'style="color: red;"' : ''; @endphp >{{ $key+1 . '. '.$val[1] }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
