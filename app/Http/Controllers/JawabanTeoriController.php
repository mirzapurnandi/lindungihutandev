<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JawabanTeoriController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        $data = [
            [
                'Jelaskan apa yang dimaksud constructor, method, class, object, variabel, parameter, function, procedure?',
                'jawaban1',
                '<b>constructor</b> merupakan suatu method yang akan memberikan nilai awal pada saat suatu objek dibuat.<br>
                <b>method</b> merupakan suatu operasi berupa fungsi-fungsi yang dapat dikerjakan oleh suatu object. <br>
                <b>class</b> suatu blueprint atau cetakan untuk menciptakan suatu instant dari object. <br>
                <b>object</b> adalah instance dari class. Jika class secara umum merepresentasikan (template) sebuah object, sebuah instance adalah representasi nyata dari class itu sendiri. <br>
                <b>variabel</b> merupakan suatu wadah atau tempat untuk menampung data berupa string, integer maupun array. <br>
                <b>parameter</b> merupakan isian yang digunakan pada sebuah function atau method. <br>
                <b>function</b> adalah Sebuah kumpulan Statement yang akan mengembalikan sebuah nilai balik pada pemanggilnya. Nilai yang dihasilkan Function harus ditampung kedalam sebuah variabel.<br>
                <b>procedure</b> adalah sub program yang digunakan untuk melakukan proses tertentu dan tidak mengembalikan nilai, bisa disimpan dalam database sebagai object skema, sehingga suatu procedure bisa digunakan berulangkali tanpa harus melakukan parsing dan compile ulang.'
            ],
            [
                'Jelaskan apa itu OOP dan apa kelebihan saat kita menggunakannya? Bahasa apa saja yang bisa menggunakan OOP?',
                'jawaban2',
                '<b>OOP (Object Oriented Programming)</b> adalah suatu metode pemrograman yang berorientasi kepada objek. Tujuan dari OOP diciptakan adalah untuk mempermudah pengembangan program dengan cara mengikuti model yang telah ada di kehidupan sehari-hari.<br>
                Kelebihan menggunakan OOP yaitu <br>1. Dapat digunakan kembali (Reusability). <br>2. Refactoring. <br> 3. Extensible. <br> 4. Pemeliharaan (Maintenance). <br> 5. Efisiensi. <br>
                Untuk penggunaan OOP semua bahasa bisa menggunakan konsep OOP (php, nodeJs, c++, dll)
                '
            ],
            [
                'Jelaskan perbedaan echo dan print, serta print_r dan var dump?',
                'jawaban3',
                '<b>echo</b> digunakan HANYA sekedar untuk mencetak output ke browser, tidak ada tujuan lain, sehingga statement inilah yang paling sering digunakan untuk mencetak output ke browser.<br>
                <b>print</b> ini akan selalu menghasilkan nilai 1. print hanya dapat menerima satu argumen, sehingga kita tidak bisa menulis: print: "Nama", $nama <br>
                <b>print_r</b> ini ditujukan untuk mencetak nilai variabel dengan format yang lebih mudah dibaca contohnya nilai array atau object.<br>
                <b>var_dump</b> merupakan fungsi, sehingga dalam penulisannya, kita harus menggunakan tanda kurung. var_dump biasanya digunakan utk debugging.'
            ],
            [
                'Jelaskan tentang if else, while, while do, switch case, for, foreach baik dari fungsi maupun penggunaan?',
                'jawaban4',
                '<b>if else</b> statement – gunakan pernyataan ini jika Anda ingin mengeksekusi sekumpulan kode saat kondisinya benar dan jika kondisinya tidak benar.<br>
                <b>Perulangan while</b> – Mengulangi blok kode selama kondisi yang ditentukan benar. Jika ekspresi tes benar maka blok kode akan dieksekusi. <br>
                <b>Do while</b> loop akan selalu mengeksekusi blok kode satu kali, kemudian akan memeriksa kondisinya, dan mengulangi loop saat kondisi yang ditentukan benar.<br>
                <b>for loop</b> dapat digunakan untuk melintasi sekumpulan kode selama beberapa kali.<br>
                <b>Perulangan foreach</b> digunakan untuk melintasi elemen array. Ini hanya bekerja pada array dan objek.'
            ],
            [
                'Apa yang terjadi jika dalam if-then-else Anda tidak menyertakan else?',
                'jawaban5',
                '<b>If then else</b> memiliki artian sebagai dua macam bentuk kondisi yang dimana akan menjadi sebuah sayat untuk dilakukan pengerjaan dari dua jenis bentuk pernyataan. Secara umum, bentuk daripada percabangan dengan menggunakan dua kondisi adalah seperti:<br>
                if <kondisi> then<br>
                pernyataan1<br>
                else<br>
                pernyataan2<br>'
            ],
            [
                'Jelaskan apa yang dimaksud array?',
                'jawaban6',
                '<b>Array</b> merupakan tipe data terstruktur dalam pemrograman, array memungkinkan untuk menyimpan data maupun referensi objek dalam jumlah banyak dan terindeks. Sehingga sejumlah variabel dapat menggunakan nama yang sama.'
            ],
            ['Jelaskan dan berikan contoh yang dimaksud dengan inheritance, polymorfis, overriding, overloading, encapsulation, abstraction', 'jawaban7', 'disini 1'],
            [
                'Jelaskan perbedaan HTTP dan HTTPS',
                'jawaban8',
                '<b>Hypertext Transfer Protocol (HTTP)</b> merupakan sebuah protokol jaringan aplikasi yang digunakan untuk mendistribusikan informasi antara server dengan client. Server disini yang dimaksud adalah jenis web server dengan bentuk fisik jaringan komputer yang memiliki kapasitas penyimpanan data berskala besar. Selanjutnya yang berperan sebagai client adalah web browser yang dapat mengakses, menerima hingga menampilkan konten web melalui browser.<br><br>
                Sedangkan <b>Hypertext Transfer Protocol Secure (HTTPS)</b> merupakan hasil pengembangan dari versi HTTP sebelumnya, HTTPS memiliki fungsi keamanan yang lebih ketat sehingga membuat client merasa aman dalam mengakses berbagai konten web. Protokol https ini dikembangkan langsung oleh perusahaan berbasis IT Netscape Communication Corp.'
            ],
            [
                'Jelaskan dan berikan contoh tentang API',
                'jawaban9',
                '<b>API atau Application Programming Interface</b> adalah sebuah interface yang dapat menghubungkan aplikasi satu dengan aplikasi lainnya. Jadi, API berperan sebagai perantara antar berbagai aplikasi berbeda, baik dalam satu platform yang sama atau lintas platform.<br>
                contohnya bisa akses website ini <a href="http://jsonplaceholder.typicode.com/users">Link</a>'
            ],
            [
                'Jelaskan yang dimaksud MVC dan HMVC?',
                'jawaban10',
                'MVC singkatan dari Model View dan Controller, ini berguna untuk merapikan kodingan, sedangkan HMVC itu Hirarky, Model, View, dan Controller untuk susunannya lebih hirarky, bisa dikatakan di dalam folder banyak folder lainnya.'
            ],
            [
                'Mengapa setiap bahasa pemrograman itu terdapat framework dan menurut Anda apa kegunaan framework?',
                'jawaban11',
                'Framework (kerangka kerja) adalah istilah yang sering muncul dalam dunia developer. Istilah tersebut memiliki fungsi yang sangat besar bagi pengembangan kode program secara sistematis. Framework sangat membantu programmer saat ini, dengan adanya framework kita tidak perlu lagi membuat kerangka dari awal'
            ],
            ['Jelaskan perbedaan method ”Post” dan “Get”? Dan bagaimana mendapatkan informasi dari form yang menggunakan metode GET dan POST?', 'jawaban12', 'disini 1'],
            ['Jelaskan konsep mengenai web service dan fungsi apa yang digunakan untuk memanggil web service client?', 'jawaban13', 'disini 1'],
            ['Apa itu mysql_fetch_Array(), mysql_fetch_row() dan mysql_fetch_assoc() dan jelaskan perbedaannya?', 'jawaban14', 'disini 1']
        ];
        return view('teori.index', [
            'result' => $data
        ]);
    }
}
