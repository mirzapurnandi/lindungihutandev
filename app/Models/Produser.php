<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produser extends Model
{
    use HasFactory;

    protected $table = "produser";

    protected $fillable = [
        'kd_produser',
        'nm_produser',
        'international'
    ];

    protected $primaryKey = 'kd_produser';

    protected $keyType = 'string';
}
