<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    use HasFactory;

    protected $table = "film";

    protected $fillable = [
        'kd_film',
        'nm_film',
        'genre',
        'artis',
        'produser',
        'pendapatan',
        'nominasi'
    ];

    protected $primaryKey = 'kd_film';

    protected $keyType = 'string';

    public function artisss()
    {
        return $this->belongsTo(Artis::class, 'artis');
    }
}
